/*
msBayes2Site.c
Copyright (C) 2015 David Rhee (david.rhee@einstein.yu.edu)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <omp.h>

#define MAXCHAR 2000

int checkOptions (int argc, char **argv, char *inputFile, char *outputFile, char *tempDirectory, int *numElements, int *jobNumber, int *simulationNumber, float *ratioNumber);
int subMain(char *inputFile, char *outputFile, char *tempDirectory, int numElements, int jobNumber, int simulationNumber, float ratioNumber);
int readintoArray (float *arrayMsp, float *arrayHpa, char *inputFile);
int writeModel (char *commandLine, float ratioNumber);
int writeScript (char *commandLine, char *tempDirectory, int jobNumber, int my_rank, int simulationNumber);
int writeData (float msp, float hpa, char *commandLine);
float returnMu (char *commandLine);
int writeArray (float *arrayScore, int numCounter, char *outputFile);

int main (int argc, char **argv) {
    char inputFile[MAXCHAR];  		/* name of input file */
    char outputFile[MAXCHAR]; 		/* name of output file */
    char tempDirectory[MAXCHAR]; 	/* name of temporary work directory */
    int numElements = 0;     		/* number of total sites */
    int jobNumber = 0;			/* job number */
    int simulationNumber = 0;		/* simulation number */
    float ratioNumber = 0;		/* msp/hpa ratio */
    int check;				/* for options */

    //parse options and proceed only if everything looks good
    if ((check = checkOptions (argc, argv, inputFile, outputFile, tempDirectory, &numElements, &jobNumber, &simulationNumber, &ratioNumber)) != 0) {
	return 1;
    }
    
    printf ("\nStarting msBayes2Site...\n");

    double dif = 0;	/* to measure total running time */
    time_t start,end;
    time(&start);

    //run openBUGS
    if (subMain(inputFile, outputFile, tempDirectory, numElements, jobNumber, simulationNumber, ratioNumber) != 0) {
	return 1;
    }
    
    time(&end);
    dif = difftime (end,start);
    printf ("It took %.2lf seconds to complete %d sites\n", dif, numElements);

    return 0;
}

int checkOptions (int argc, char **argv, char *inputFile, char *outputFile, char *tempDirectory, int *numElements, int *jobNumber, int *simulationNumber, float *ratioNumber) {
    
    //Check to make sure all options are there
    if (argc != 15) {
	printf("Usage:\n");
	printf("msBayes2Site -i input_filename -o output_filename -t tempDirectory -n number_of_sites -j job_number -s simulation_number -r ratio_number\n");
	return 1;
    }

    int c;
    int check = 0; /* to store option status */

    while ((c = getopt (argc, argv, ":i:o:t:n:j:s:r:")) != -1) {
	switch (c) {
	    case 'i':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Input file name too long\n");
		} else {
		    strcpy(inputFile, optarg);
		    check = check + 1;    
		}
		break;
	    case 'o':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Output file name too long\n");
		} else {
		    strcpy(outputFile, optarg);
		    check = check + 10;
		}
		break;
	    case 't':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Name of temporary Directory too long\n");
		} else {
		    strcpy(tempDirectory, optarg);
		    check = check + 100;
		}
		break;
	    case 'n':
		sscanf(optarg,"%d",numElements);
		check = check + 1000;
		break;
	    case 'j':
		sscanf(optarg,"%d",jobNumber);
		check = check + 10000;
		break;
	    case 's':
		sscanf(optarg,"%d",simulationNumber);
		check = check + 100000;
		break;
	    case 'r':
		sscanf(optarg,"%f",ratioNumber);
		check = check + 1000000;
		break;
	}
    }
    
    // do some error checking to ensure all options are set
    if (check != 1111111) {
	printf("Not all options were properly set.\n");
	return 1;
    }
    if ((*numElements < 0) || (*numElements > 5000000)) {
	printf("Number of sites must be 0 - 5000000\n");
	return 1;
    }
    if ((*simulationNumber < 0) || (*simulationNumber > 100000)) {
	printf("Simulation number must be 0 - 100000\n");
	return 1;
    }
    if (*ratioNumber <= 0) {
	printf("Ratio number must be greater than 0\n");
	return 1;
    }

    return 0;
}

int subMain(char *inputFile, char *outputFile, char *tempDirectory, int numElements, int jobNumber, int simulationNumber, float ratioNumber) {
    int numCounter = 0;
    int i = 0;
    int k;
    
    //Command line scripts
    char commandLine[MAXCHAR];

    //Allocate dynamic arrays
    float *arrayMsp;
    float *arrayHpa;
    float *arrayScore;
    arrayMsp = (float *) malloc((numElements + 1) * sizeof(float));
    arrayHpa = (float *) malloc((numElements + 1) * sizeof(float));
    arrayScore = (float *) malloc((numElements + 1) * sizeof(float));
    
    //read data into arrays
    numCounter = readintoArray(arrayMsp, arrayHpa, inputFile);
    if (numCounter == 0) {
	printf ("Error reading in input data\n");
	return 1;
    }
    printf ("Finished reading in inputfile %s.\n", inputFile);

    //write model
    k = snprintf(commandLine, sizeof(commandLine)-1, "%s/model.site.%d.txt", tempDirectory, jobNumber);
    if (k <= 0) { //buffer overrun
	printf ("Model commandline problem -- %s\n", commandLine);
	return 1;
    }
    if (writeModel(commandLine, ratioNumber) != 0) {
	printf ("Error writing model\n");
	return 1;	
    }
    printf ("Finished writing openbug model for job-%d.\n", jobNumber);
    
    //Starting parallel jobs
    #pragma omp parallel shared(arrayMsp, arrayHpa, arrayScore, numCounter) private(i, k, commandLine)
    {
	int my_rank = omp_get_thread_num();
	
	//write script.txt
	k = snprintf(commandLine, sizeof(commandLine)-1, "%s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	writeScript(commandLine, tempDirectory, jobNumber, my_rank, simulationNumber);
	printf ("Finished writing script files for job-%d rank-%d.\n", jobNumber, my_rank);
	#pragma omp barrier
	
	#pragma omp for
	for(i = 0; i < numCounter; i++) {	    
	    //Write data.txt
	    k = snprintf(commandLine, sizeof(commandLine)-1, "%s/data-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    writeData(arrayMsp[i], arrayHpa[i], commandLine);
	    
	    //Call Openbugs
	    k = snprintf(commandLine, sizeof(commandLine)-1, "~/bin/OpenBUGS %s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    arrayScore[i] = (1-returnMu(commandLine));    
	    
	    //Remove data.txt
	    k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/data-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    system(commandLine);
	}
	
	//Remove script.txt
	k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	system(commandLine);
	printf ("Removed script files for job-%d rank-%d.\n", jobNumber, my_rank);
	#pragma omp barrier
    }
    
    //delete model
    k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/model.site.%d.txt", tempDirectory, jobNumber);
    if (k <= 0) { //buffer overrun
	printf ("Model commandline buffer problem -- %s\n", commandLine);
	return 1;
    }
    system(commandLine);
    printf ("Removed openbug model for job-%d.\n", jobNumber);
    
    //write to output.txt
    if (writeArray (arrayScore, numCounter, outputFile) != 0) {
	printf ("Error writing output\n");
	return 1;	
    }
    printf ("Finished writing to outputfile %s.\n", outputFile);
    
    //Free allocated arrays
    free (arrayMsp);
    free (arrayHpa);
    free (arrayScore);
    
    return 0;
}

int readintoArray (float *arrayMsp, float *arrayHpa, char *inputFile) {
    FILE *fp = fopen ( inputFile, "r" );
    int numCounter = 0;
    
    if (fp == NULL) {
    	printf("Error in opening inputFile..\n\n");
    	return 0;
    } else {
	while(!feof(fp)){
	    fscanf(fp,"%f\t%f\n", &arrayMsp[numCounter], &arrayHpa[numCounter]);
	    numCounter++;
        }
        fclose(fp);
    }
    return numCounter;
}

int writeModel (char *commandLine, float ratioNumber) { 
    //write model.txt
    FILE *q = fopen(commandLine, "w");
    if (q== NULL) {
  	printf("Error in opening model file..\n");
	printf("%s\n", commandLine);
	return 1;
    } else {		
	fprintf(q, "model {\n x1 ~  dpois(lambdat1)\n y1 ~  dpois(lambda1)\n");
	fprintf(q, "lambda1 <- lambdat1/%f*mu1\n", ratioNumber);
	fprintf(q, "lambdat1 <- lambdag1\n lambdag1 ~ dgamma(gr1,pf1)\n gr1 <- 200*r1\n r1 ~dbeta(1,1)\n");
	fprintf(q, "pf1 <- (1-p1)/p1\n p1 ~dbeta(1,1)\n mu1 ~  dbeta(1,1)\n }\n");
  	fclose(q);
    }   
    return 0;
}

int writeScript (char *commandLine, char *tempDirectory, int jobNumber, int my_rank, int simulationNumber) {
    //write script.txt
    FILE *q = fopen(commandLine, "w");
    if (q== NULL) {
  	printf("Error in opening script file..");
	printf("%s\n", commandLine);
	return 1;
    } else {		
	fprintf(q, "modelCheck(\'%s/model.site.%d.txt\')\n", tempDirectory, jobNumber);
	fprintf(q, "modelData(\'%s/data-%d-%d.txt\')\n", tempDirectory, jobNumber, my_rank);
	fprintf(q, "modelCompile(3)\nmodelGenInits()\nmodelUpdate(1000)\nsamplesSet(\'mu1\')\n");
	fprintf(q, "modelUpdate(%d)\nsamplesStats(\'*\')\n", simulationNumber);
  	fclose(q);
    }   
    return 0;
}

int writeData (float msp, float hpa, char *commandLine) {
    //write data.txt
    FILE *p = fopen(commandLine, "w");
    if (p== NULL) {
  	printf("Error in opening data file..\n");
	printf("%s\n", commandLine);
	return 1;
    } else {
	fprintf(p, "list(x1 = %f, y1 = %f)", msp, hpa);
  	fclose(p);
    }  
    return 0;
}

float returnMu (char *commandLine) {
    FILE *fp;
    char buffer[MAXCHAR];
    char temp[MAXCHAR/2];
    int counter = 0;
    float returnValue = 0;
    
    fp = popen(commandLine, "r");
    if (fp == NULL) {
    	printf("Failed to run openbug\n");
	printf("%s\n", commandLine);
    	return 0;
    } else {
	sleep(1); /* make sure program finishes running */
	while(!feof(fp)){
	    if(fgets(buffer, sizeof(buffer)-1, fp) != NULL) {
		if (counter == 10) {
		    sscanf(buffer, "\t%s\t%f", temp, &returnValue);
		}
		counter++;
	    }  
        }
        pclose(fp);
    }
    if (counter == 0) { /*make sure program returned output of known pattern*/
	printf("Something went wrong with openbug\n");
	printf("%s\n", commandLine);
	return 0;
    }
    return returnValue;
}

int writeArray (float *arrayScore, int numCounter, char *outputFile) {
    FILE *p = fopen(outputFile, "w");
    if (p== NULL) {
  	printf("Error in opening outputFile..\n");
	return 1;
    } else {
	int i;
	for(i = 0; i < numCounter; i++) {
	    fprintf(p, "%f\n", arrayScore[i]);
	}
	fclose(p);
    }
    return 0;
}
