_____________________________________________________________
_____________________________________________________________
			msBayes2
_____________________________________________________________
_____________________________________________________________



CONTENT
_______________________________

License.txt
Makefile
msBayes2.py
msBayes2Region.c
msBayes2Site.c
masako.hes.hg19H.chr1.hcount		=> Human Chromosome 1 hpaII library
masako.hes.hg19M.chr1.hcount		=> Human Chromosome 1 mspI library
masako.hes.hg19H.chr7.500.hcount	=> Human Chromosome 7 hpaII library - first 500 sites
masako.hes.hg19M.chr7.500.hcount	=> Human Chromosome 7 mspI library - first 500 sites
OpenBUGS-3.2.3.tar.gz
readme.txt



OpenBUGS
_______________________________

Download the OpenBUGS package at http://www.openbugs.net/w/FrontPage
msBayes2 has been tested with OpenBUGS release 3.2.3

Install OpenBUGS with following command
./configure --prefix=$HOME
make
make install

$HOME should be your home directory
This will install OpenBUGS at ~/bin
or
You can install elsewhere but be sure to change the line 233 in msBayes2Region.c and msBayes2Site.c to where OpenBUGS is installed.
i.e. 
If installed at /usr/bin, change line 233 in msBayes2Region.c to "/usr/bin/OpenBUGS %s/script-%d-%d.txt"
If installed at /usr/bin, change line 202 in msBayes2Site.c to "/usr/bin/OpenBUGS %s/script-%d-%d.txt"



Installation (Linux / Mac OS X)
_______________________________

The binary must be built using GCC version 4.2 or greater in order to use the openMP library.

Install msBayes2Region and msBayes2Site with following command
make all



Python
--------

Release 2.7 or higher required for argparse module



Commandline Parameters
_______________________________

'export OMP_NUM_THREADS=$NUMTHREADS' => $NUMTHREADS should be the number of CPU cores available
'python msBayes2.py -p project_name -s scratch_space -w working_folder -f mspi_file hpaii_file -d <optional> -b number_of_simulation'

-p project_name => Project name must be alphanumeric (with exception to dot . and underscore _ ), minimum of 6 and maximum of 15 characters. 
-s scratch_space => Name of fast scratch space but can be any directory. Usually '/tmp'
-w working_directory => Where input files live and output file will be written to.
-f mspi_file hpaii_file => Name of mspI and hpaII files
-d => Optional flag to use site information only available in hpaII file.
-b number_of_simulation => OpenBUGS option for number of simulation. Usually set to 3000.
-r number_of_simulation, number_of_base_pair_between_sites => OpenBUGS option for number of simulation. Usually set to 3000. Number of base pair between sites to determine regions.


Licence
_______________________________

msBayes2
Copyright (C) 2015 David Rhee (david.rhee@einstein.yu.edu)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
