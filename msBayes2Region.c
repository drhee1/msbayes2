/*
msBayes2Region.c
Copyright (C) 2015 David Rhee (david.rhee@einstein.yu.edu)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <omp.h>

#define MAXCHAR 2000
#define MAXCHARTWO 5000

int checkOptions (int argc, char **argv, char *inputFileCount, char *inputFilePosition, char *outputFile, char *tempDirectory, int *numElements, int *numRegions, int *jobNumber, int *simulationNumber, float *ratioNumber);
int subMain(char *inputFileCount, char *inputFilePosition, char *outputFile, char *tempDirectory, int numElements, int numRegions, int jobNumber, int simulationNumber, float ratioNumber);
int readintoArrayCount (float *arrayMsp, float *arrayHpa, char *inputFileCount);
int readintoArrayPosition (int *arrayStart, int *arrayEnd, char *inputFilePosition);
int writeModel (char *commandLine, float ratioNumber);
int writeScript (char *commandLineTwo, char *tempDirectory, int jobNumber, int my_rank, int simulationNumber);
int writeData (int startPosition, int endPosition, float *arrayMsp, float *arrayHpa, char *commandLine);
float returnMu (char *commandLine);
int writeArray (float *arrayScore, int numCounter, char *outputFile);

int main (int argc, char **argv) {
    char inputFileCount[MAXCHAR];  	/* name of input count file */
    char inputFilePosition[MAXCHAR];  	/* name of input position file */
    char outputFile[MAXCHAR]; 		/* name of output file */
    char tempDirectory[MAXCHARTWO]; 	/* name of temporary directory */
    int numElements = 0;     		/* number of total sites */
    int numRegions = 0;     		/* number of total regions */
    int jobNumber = 0;			/* job number */
    int simulationNumber = 0;		/* simulation number */
    float ratioNumber = 0;		/* msp/hpa ratio */
    int check;				/* for options */

    //parse options and proceed only if everything looks good
    if ((check = checkOptions (argc, argv, inputFileCount, inputFilePosition, outputFile, tempDirectory, &numElements, &numRegions, &jobNumber, &simulationNumber, &ratioNumber)) != 0) {
	return 1;
    }
    
    printf ("\nStarting msBayes2Region\n");

    double dif = 0;	/* to measure total running time */
    time_t start,end;
    time(&start);

    //run openBUGS
    if (subMain(inputFileCount, inputFilePosition, outputFile, tempDirectory, numElements, numRegions, jobNumber, simulationNumber, ratioNumber) != 0) {
	return 1;
    }
    
    time(&end);
    dif = difftime (end,start);
    printf ("It took %.2lf seconds to complete %d regions\n", dif, numRegions);
    
    return 0;
}

int checkOptions (int argc, char **argv, char *inputFileCount, char *inputFilePosition, char *outputFile, char *tempDirectory, int *numElements, int *numRegions, int *jobNumber, int *simulationNumber, float *ratioNumber) {
    
    //Check to make sure all options are there
    if (argc != 19) {
	printf("Usage:\n");
	printf("msBayes2Region -i count_input_filename -p position_input_filename -o output_filename -t tempDirectory -n number_of_sites -g number_of_regions -j job_number -s simulation_number -r ratio_number\n");
	return 1;
    }

    int c;
    int check = 0; /* to store option status */
  
    while ((c = getopt (argc, argv, ":i:p:o:t:n:g:j:s:r:")) != -1) {
	switch (c) {
	    case 'i':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Count input file name too long\n");
		} else {
		    strcpy(inputFileCount, optarg);
		    check = check + 1;    
		}
		break;
	    case 'p':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Position input file name too long\n");
		} else {
		    strcpy(inputFilePosition, optarg);
		    check = check + 10;    
		}
		break;
	    case 'o':
		if(strlen(optarg) > MAXCHAR) {
		    printf("Output file name too long\n");
		} else {
		    strcpy(outputFile, optarg);
		    check = check + 100;
		}
		break;
	    case 't':
		if(strlen(optarg) > MAXCHARTWO) {
		    printf("Name of temporary Directory too long\n");
		} else {
		    strcpy(tempDirectory, optarg);
		    check = check + 1000;
		}
		break;
	    case 'n':
		sscanf(optarg,"%d",numElements);
		check = check + 10000;
		break;
	    case 'g':
		sscanf(optarg,"%d",numRegions);
		check = check + 100000;
		break;
	    case 'j':
		sscanf(optarg,"%d",jobNumber);
		check = check + 1000000;
		break;
	    case 's':
		sscanf(optarg,"%d",simulationNumber);
		check = check + 10000000;
		break;
	    case 'r':
		sscanf(optarg,"%f",ratioNumber);
		check = check + 100000000;
		break;
	}
    }

    // do some error checking to ensure all options are set
    if (check != 111111111) {
	printf("Not all options were properly set.\n");
	return 1;
    }
    if ((*numElements < 0) || (*numElements > 5000000)) {
	printf("Number of sites must be 0 - 5000000\n");
	return 1;
    }
    if ((*simulationNumber < 0) || (*simulationNumber > 100000)) {
	printf("Simulation number must be 0 - 100000\n");
	return 1;
    }
    if (*numRegions <= 0) {
	printf("Number of regions must be greater than 0\n");
	return 1;
    }
    if (*ratioNumber <= 0) {
	printf("Ratio number must be greater than 0\n");
	return 1;
    }

    return 0;
}

int subMain(char *inputFileCount, char *inputFilePosition, char *outputFile, char *tempDirectory, int numElements, int numRegions, int jobNumber, int simulationNumber, float ratioNumber) {
    int numCounter = 0;
    int i = 0;
    int k;

    //Command line scripts
    char commandLine[MAXCHAR];

    //Allocate dynamic arrays
    float *arrayMsp;
    float *arrayHpa;
    float *arrayScore;
    int *arrayStart;
    int *arrayEnd;
    arrayMsp = (float *) malloc(numElements * sizeof(float));
    arrayHpa = (float *) malloc(numElements * sizeof(float));
    arrayScore = (float *) malloc(numRegions * sizeof(float));
    arrayStart = (int *) malloc(numRegions * sizeof(int));
    arrayEnd = (int *) malloc(numRegions * sizeof(int));
    
    //read data into arrays
    if (readintoArrayCount(arrayMsp, arrayHpa, inputFileCount) == 0) {
	printf ("Error reading in input count data\n");
	return 1;
    }
    printf ("Finished reading in count inputfile %s.\n", inputFileCount);

    numCounter = readintoArrayPosition(arrayStart, arrayEnd, inputFilePosition);
    if (numCounter == 0) {
	printf ("Error reading in input position data\n");
	return 1;
    }
    printf ("Finished reading in position inputfile %s.\n", inputFilePosition);
    
    //write model
    k = snprintf(commandLine, sizeof(commandLine)-1, "%s/model.region.%d.txt", tempDirectory, jobNumber);
    if (k <= 0) { //buffer overrun
	printf ("Model commandline problem -- %s\n", commandLine);
	return 1;
    }
    if (writeModel(commandLine, ratioNumber) != 0) {
	printf ("Error writing model\n");
	return 1;	
    }
    printf ("Finished writing openbug model for job-%d.\n", jobNumber);    
    
    
    //Starting parallel jobs
    #pragma omp parallel shared(arrayMsp, arrayHpa, arrayStart, arrayEnd, arrayScore, numCounter) private(i, k, commandLine)
    {
	int my_rank = omp_get_thread_num();
	
	//write script.txt
	k = snprintf(commandLine, sizeof(commandLine)-1, "%s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	writeScript(commandLine, tempDirectory, jobNumber, my_rank, simulationNumber);
	printf ("Finished writing script files for job-%d rank-%d.\n", jobNumber, my_rank);
	#pragma omp barrier

	#pragma omp for
	for(i = 0; i < numCounter; i++) {	    
	    //Write data.txt
	    k = snprintf(commandLine, sizeof(commandLine)-1, "%s/data-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    writeData(arrayStart[i], arrayEnd[i], arrayMsp, arrayHpa, commandLine);
	    
	    //Call Openbugs
	    k = snprintf(commandLine, sizeof(commandLine)-1, "~/bin/OpenBUGS %s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    arrayScore[i] = (1-returnMu(commandLine));    
	    
	    //Remove data.txt
	    k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/data-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	    system(commandLine);
	}
	
	//Remove script.txt
	k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/script-%d-%d.txt", tempDirectory, jobNumber, my_rank);
	system(commandLine);
	printf ("Removed script files for job-%d rank-%d.\n", jobNumber, my_rank);
	#pragma omp barrier
    }
    
    //delete model
    k = snprintf(commandLine, sizeof(commandLine)-1, "rm %s/model.region.%d.txt", tempDirectory, jobNumber);
    if (k <= 0) { //buffer overrun
	printf ("Model commandline buffer problem -- %s\n", commandLine);
	return 1;
    }
    system(commandLine);
    printf ("Removed openbug model for job-%d.\n", jobNumber);
    
    //write to output.txt
    if (writeArray(arrayScore, numCounter, outputFile) != 0) {
	printf ("Error writing output\n");
	return 1;	
    }
    printf ("Finished writing to outputfile %s.\n", outputFile);
    
    //Free allocated arrays
    free (arrayStart);
    free (arrayEnd);
    free (arrayMsp);
    free (arrayHpa);
    free (arrayScore);
    
    return 0;
}

int readintoArrayCount (float *arrayMsp, float *arrayHpa, char *inputFileCount) {
    FILE *fp = fopen ( inputFileCount, "r" );
    int numCounter = 0;
    
    if (fp == NULL) {
    	printf("Error in opening inputFile..\n\n");
    	return 0;
    } else {
	while(!feof(fp)){
	    fscanf(fp,"%f\t%f\n", &arrayMsp[numCounter], &arrayHpa[numCounter]);
	    numCounter++;
        }
        fclose(fp);
    }
    return numCounter;
}

int readintoArrayPosition (int *arrayStart, int *arrayEnd, char *inputFilePosition) {
    FILE *fp = fopen ( inputFilePosition, "r" );
    int numCounter = 0;
    
    if (fp == NULL) {
    	printf("Error in opening inputPositionFile..\n\n");
    	return 0;
    } else {
	while(!feof(fp)){
	    fscanf(fp,"%d\t%d\n", &arrayStart[numCounter], &arrayEnd[numCounter]);
	    numCounter++;
        }
        fclose(fp);
    }
    return numCounter;
}

int writeModel (char *commandLine, float ratioNumber) { 
    //write model.txt
    FILE *q = fopen(commandLine, "w");
    if (q== NULL) {
  	printf("Error in opening model file..\n");
	printf("%s\n", commandLine);
	return 1;
    } else {
	fprintf(q, "model {\n for( i in 1 : N ) {\n x1[i] ~  dpois(lambdat1[i])\n y1[i] ~  dpois(lambda1[i])\n");
	fprintf(q,  "lambda1[i] <- lambdat1[i]/%f*mu1\n", ratioNumber);
	fprintf(q, "lambdat1[i] <- lambdag1[i]\n lambdag1[i] ~ dgamma(gr1[i],pf1[i])\n gr1[i] <- 200*r1[i]\n");
	fprintf(q, "r1[i] ~dbeta(1,1)\n pf1[i] <- (1-p1[i])/p1[i]\n p1[i] ~dbeta(1,1)\n }\n mu1 ~ dbeta(1,1)\n }\n");
  	fclose(q);
    }   
    return 0;
}

int writeScript (char *commandLine, char *tempDirectory, int jobNumber, int my_rank, int simulationNumber) {
    //write script.txt
    FILE *q = fopen(commandLine, "w");
    if (q== NULL) {
  	printf("Error in opening script file..");
	printf("%s\n", commandLine);
	return 1;
    } else {		
	fprintf(q, "modelCheck(\'%s/model.region.%d.txt\')\n", tempDirectory, jobNumber);
	fprintf(q, "modelData(\'%s/data-%d-%d.txt\')\n", tempDirectory, jobNumber, my_rank);
	fprintf(q, "modelCompile(3)\nmodelGenInits()\nmodelUpdate(1000)\nsamplesSet(\'mu1\')\n");
	fprintf(q, "modelUpdate(%d)\nsamplesStats(\'*\')\n", simulationNumber);
  	fclose(q);
    }   
    return 0;
}

int writeData (int startPosition, int endPosition, float *arrayMsp, float *arrayHpa, char *commandLine) {
    //write data.txt
    FILE *p = fopen(commandLine, "a");
    if (p== NULL) {
  	printf("Error in opening data file..\n");
	printf("%s\n", commandLine);
	return 1;
    } else {
	int x;
	
	//if one site
	if (startPosition == endPosition) {
	    fprintf(p, "list(N = 1 , x1=c(%f), y1=c(%f))", arrayMsp[startPosition], arrayHpa[startPosition]);
	} else { //more than one site per region

	    fprintf(p, "list(N = %d , x1=c(%f", ((endPosition-startPosition)+1), arrayMsp[startPosition]);
	    for (x = (startPosition+1); x < (endPosition+1); x++) {
		fprintf(p, ",%f", arrayMsp[x]);
	    }
	    
	    fprintf(p, "), y1=c(%f", arrayHpa[startPosition]);
	    for (x = (startPosition+1); x < (endPosition+1); x++) {
		fprintf(p, ",%f", arrayHpa[x]);
	    }
	    fprintf(p, "))");
	}
  	fclose(p);
    }  
    return 0;
}

float returnMu (char *commandLine) {
    FILE *fp;
    char buffer[MAXCHAR];
    char temp[MAXCHAR/2];
    int counter = 0;
    float returnValue = 0;
    
    fp = popen(commandLine, "r");
    if (fp == NULL) {
    	printf("Failed to run openbug\n");
	printf("%s\n", commandLine);
    	return 0;
    } else {
	sleep(1); /* make sure program finishes running */
	while(!feof(fp)){
	    if(fgets(buffer, sizeof(buffer)-1, fp) != NULL) {
		if (counter == 10) {
		    sscanf(buffer, "\t%s\t%f", temp, &returnValue);
		}
		counter++;
	    }  
        }
        pclose(fp);
    }
    if (counter == 0) { /*make sure program returned output of known pattern*/
	printf("Something went wrong with openbug\n");
	printf("%s\n", commandLine);
	return 0;
    }
    return returnValue;
}

int writeArray (float *arrayScore, int numCounter, char *outputFile) {
    FILE *p = fopen(outputFile, "w");
    if (p== NULL) {
  	printf("Error in opening outputFile..\n");
	return 1;
    } else {
	int i;
	for(i = 0; i < numCounter; i++) {
	    fprintf(p, "%f\n", arrayScore[i]);
	}
	fclose(p);
    }
    return 0;
}
