#!/usr/bin/env python

''' 
msBayes2.py

USEAGE: python msBayes2.py -options <addtional parameters> 
PURPOSE: parse and determines methylation values from HELP-tagging or Methyl-seq assays
DATE: 12/24/2014
Version: 0.1
AUTHOR: David Rhee
Modifier: David Rhee
Email: david.rhee@einstein.yu.edu

'''

import sys, string, commands, time, argparse
import traceback, types, re, random

########################################################################################################################
########################################################################################################################
# Helper Functions
########################################################################################################################
# Run msBayes2Site.c
def msBayes2Site(infoList, hcountListNumbers, number_of_simulations) :
    print("Running msBayes2Site...")

    #finish writing script call and submit
    programCall = './msBayes2Site -i ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.count.txt'
    programCall = programCall + ' -o ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.msBayes2Site.txt'
    programCall = programCall + ' -t "' + infoList["scratch"] + '"'
    programCall = programCall + ' -n ' + str(hcountListNumbers["totalCount"])
    programCall = programCall + ' -j ' + str(hcountListNumbers["jobNumber"])
    programCall = programCall + ' -s ' + str(number_of_simulations)
    programCall = programCall + ' -r ' + str(hcountListNumbers["mspTotal"]/hcountListNumbers["hpaTotal"])
    print(programCall)
    statusCommands = commands.getstatusoutput(programCall)
    if statusCommands[0] == 0 :
	print(statusCommands[1])
    else :
	print("Error running msBayes2Site\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #combine result files
    cmd = 'paste ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.meta.txt ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.msBayes2Site.txt > ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Site.txt'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Site\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #rename the resulting file
    cmd = 'mv ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Site.txt ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Site.bed'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Site\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #gzip the resulting file
    cmd = 'gzip ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Site.bed'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Site\n\n")
	print(statusCommands[1])
	sys.exit(1)

    print("Finished msBayes2Site")

# Run msBayes2Region.c
def msBayes2Region(infoList, hcountList, hcountListNumbers, number_of_simulations, region_distance) :
    print("Running msBayes2Region...")
    
    #create regional files
    findAndWriteRegions(infoList["temporary"], infoList["projectName"], hcountList, hcountListNumbers, region_distance)
    
    #finish writing script call and submit
    programCall = './msBayes2Region -i ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.count.txt'
    programCall = programCall + ' -p ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.region.count.txt'
    programCall = programCall + ' -o ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.msBayes2Region.txt'
    programCall = programCall + ' -t "' + infoList["scratch"] + '"'
    programCall = programCall + ' -n ' + str(hcountListNumbers["totalCount"])
    programCall = programCall + ' -g ' + str(hcountListNumbers["regionCount"])
    programCall = programCall + ' -j ' + str(hcountListNumbers["jobNumber"])
    programCall = programCall + ' -s ' + str(number_of_simulations)
    programCall = programCall + ' -r ' + str(hcountListNumbers["mspTotal"]/hcountListNumbers["hpaTotal"])
    print(programCall)
    statusCommands = commands.getstatusoutput(programCall)
    if statusCommands[0] == 0 :
	print(statusCommands[1])
    else :
	print("Error running msBayes2Region\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #combine result file and delete
    cmd = 'paste ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.region.meta.txt ' + infoList["temporary"] + 'temp.' + infoList["projectName"] + '.msBayes2Region.txt > ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Region.txt'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Region\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #rename the resulting file
    cmd = 'mv ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Region.txt ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Region.bed'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Region\n\n")
	print(statusCommands[1])
	sys.exit(1)

    #gzip the resulting file
    cmd = 'gzip ' + infoList["temporary"] + infoList["projectName"] + '.msBayes2Region.bed'
    statusCommands = commands.getstatusoutput(cmd)
    if statusCommands[0] == 1 :
	print("Error running msBayes2Region\n\n")
	print(statusCommands[1])
	sys.exit(1)

    print("Finished msBayes2Region")

# Check options
def checkArguments(args) :
    if args.project and args.scratch and args.work and args.files :
	return 0

    print "error processing options : use -h for help\n"
    return 1  
    
# Generate random numbers for job number
def randomJob() :
    a = int(10000000*random.random())
    b = int(1000000*random.random())
    c = int(100000*random.random())
    d = int(10000*random.random())
    e = int(1000*random.random())
    f = int(100*random.random())
    g = int(10*random.random())
    return (a+b+c+d+e+f+g+g)

# Adjust ChrPos formatted with 15 decimals
def leadingzero(x) :
    return str(x).rjust(15, '0')

# Check if first chrNo-Pos falls within second chrNo-Pos-Pos
def inbetween(x, y) :
    chrNo,chrPos = x.split('-')
    chrNo2, fromP,toP = y.split('-')
       
    if chrNo == chrNo2 :
	if int(chrPos) >= int(fromP) and int(chrPos) <= int(toP) :
	    return 1
    return 0

# Check if chromosome id is within chr1-22|x|y|m
def check_file_line_chr (line) :
    m = re.match(r"chr[1-9]|[1][0-9]|[2][0-2]|[xymXYM]", line)
    if m :
	return 0
    return 1

# Check if file is from einstein (id chr pos + - total)
def check_file_line_five (line) :
    m = re.match(r"(\S*)\s(\S*)\s(\S*)\s(\S*)\s(\S*)\s(\S*)\n", line)
    if m:
	if str(m.group(3).isdigit()) and str(m.group(4).isdigit()) and str(m.group(5).isdigit()) and str(m.group(6).isdigit()) :
	    if check_file_line_chr (m.group(2)) == 0 :
		return 0
    return 1

# Check if file is simple (chr pos total)
def check_file_line_two (line) :
    m = re.match(r"(\S*)\s(\S*)\s(\S*)\n", line)
    if m:
	if str(m.group(2).isdigit()) and str(m.group(3).isdigit()) :
	    if check_file_line_chr (m.group(1)) == 0 :
		return 0
    return 1

# Check the identity of file
def check_file_identify (inputFile) :  
    infile = open(inputFile)
    raw1 = infile.readlines()
    infile.close()

    #check if this is coming from einstein
    if check_file_line_five(raw1[1]) == 0 :
	for row in range(2,len(raw1)):
	    if check_file_line_five(raw1[row]) == 1 :
		return 1
	    return 5
    elif check_file_line_two(raw1[1]) == 0 :
	for row in range(2,len(raw1)):
	    if check_file_line_two(raw1[row]) == 1 :
		return 1
	    return 2
    else :
	return 1

# Read Msp and Hpa hcount file and store in hcountList : Einstein format
def readMspHpaEinstein(mspFile, hpaFile, hcountList, hcountListNumbers) :
    
    #For MspI
    infile = open(mspFile)
    raw1 = infile.readlines()
    infile.close()
    
    for row in range(1,len(raw1)):
	chrPos = string.split(string.strip(raw1[row]))[1] + "-" + leadingzero(int(string.split(string.strip(raw1[row]))[2]))
	hcountList[chrPos] = string.split(string.strip(raw1[row]))[5] + "\t0"
	hcountListNumbers["mspTotal"] = hcountListNumbers["mspTotal"] + float(string.split(string.strip(raw1[row]))[5])
	hcountListNumbers["mspCount"] += 1
	hcountListNumbers["totalCount"] += 1
    
    #For HpaII
    infile = open(hpaFile)
    raw2 = infile.readlines()
    infile.close()
    
    for row2 in range(1,len(raw2)):
	chrPos2 = string.split(string.strip(raw2[row2]))[1] + "-" + leadingzero(int(string.split(string.strip(raw2[row2]))[2]))
	if hcountList.has_key(chrPos2):
	    previous = string.split(string.strip(hcountList[chrPos2]))[0]
	    hcountList[chrPos2] = previous + "\t" + string.split(string.strip(raw2[row2]))[5]
	else:
	    hcountList[chrPos2] = "0\t" + string.split(string.strip(raw2[row2]))[5]
	    hcountListNumbers["totalCount"] += 1
	hcountListNumbers["hpaTotal"] = hcountListNumbers["hpaTotal"] + float(string.split(string.strip(raw2[row2]))[5])
	hcountListNumbers["hpaCount"] += 1

# Read Msp and Hpa hcount file and store in hcountList : Simple format
def readMspHpaSimple(mspFile, hpaFile, hcountList, hcountListNumbers) :
    
    #For MspI
    infile = open(mspFile)
    raw1 = infile.readlines()
    infile.close()
    
    for row in range(1,len(raw1)):
	chrPos = string.split(string.strip(raw1[row]))[0] + "-" + leadingzero(int(string.split(string.strip(raw1[row]))[1]))
	hcountList[chrPos] = string.split(string.strip(raw1[row]))[2] + "\t0"
	hcountListNumbers["mspTotal"] = hcountListNumbers["mspTotal"] + float(string.split(string.strip(raw1[row]))[2])
	hcountListNumbers["mspCount"] += 1
	hcountListNumbers["totalCount"] += 1
    
    #For HpaII
    infile = open(hpaFile)
    raw2 = infile.readlines()
    infile.close()
    
    for row2 in range(1,len(raw2)):
	chrPos2 = string.split(string.strip(raw2[row2]))[0] + "-" + leadingzero(int(string.split(string.strip(raw2[row2]))[1]))
	if hcountList.has_key(chrPos2):
	    previous = string.split(string.strip(hcountList[chrPos2]))[0]
	    hcountList[chrPos2] = previous + "\t" + string.split(string.strip(raw2[row2]))[2]
	else:
	    hcountList[chrPos2] = "0\t" + string.split(string.strip(raw2[row2]))[2]
	    hcountListNumbers["totalCount"] += 1
	hcountListNumbers["hpaTotal"] = hcountListNumbers["hpaTotal"] + float(string.split(string.strip(raw2[row2]))[2])
	hcountListNumbers["hpaCount"] += 1

# Write Msp and Hpa hcount as separate files
def writeMspHpaCounts(temporary, projectName, hcountList) :
    metaTag = temporary + 'temp.%s.meta.txt'%projectName
    countTag = temporary + 'temp.%s.count.txt'%projectName
    outfile1 = open(metaTag, 'w')
    outfile2 = open(countTag, 'w')

    keys = hcountList.keys()
    keys.sort()

    for key in keys:
	chrNo, chrPos = key.split("-")
	msp, hpa = hcountList[key].split("\t")

	if (float(msp) == 0 and float(hpa) == 0) :
	    #if both msp and hpa values are zero
	    #do nothing
	    msp = 0
	else:
	    outfile1.write(chrNo)
	    outfile1.write('\t')
	    outfile1.write(str(int(chrPos)))
	    outfile1.write('\t')
	    outfile1.write(str(int(chrPos)+1))
	    outfile1.write('\t')
	    outfile1.write(projectName)
	    outfile1.write('\n')
	    outfile2.write(msp)
	    outfile2.write('\t')
	    outfile2.write(hpa)
	    outfile2.write('\n')
    
    outfile1.close()
    outfile2.close()

# Write Msp and Hpa hcount as separate files
def writeMspHpaCountsFiltered(temporary, projectName, hcountList, hcountListNumbers) :
    metaTag = temporary + 'temp.%s.meta.txt'%projectName
    countTag = temporary + 'temp.%s.count.txt'%projectName
    outfile1 = open(metaTag, 'w')
    outfile2 = open(countTag, 'w')

    tmp_count = 0
    tmp_hcountList = {}

    keys = hcountList.keys()
    keys.sort()

    for key in keys:
	chrNo, chrPos = key.split("-")
	msp, hpa = hcountList[key].split("\t")

	#make sure both msp and hpa are not zero
	if (float(msp) != 0 and float(hpa) != 0) :
	    #also ignore if hpa is zero
	    if (float(hpa) != 0) :
		outfile1.write(chrNo)
		outfile1.write('\t')
		outfile1.write(str(int(chrPos)))
		outfile1.write('\t')
		outfile1.write(str(int(chrPos)+1))
		outfile1.write('\t')
		outfile1.write(projectName)
		outfile1.write('\n')
		outfile2.write(msp)
		outfile2.write('\t')
		outfile2.write(hpa)
		outfile2.write('\n')
		
		tmp_count += 1
		tmp_hcountList[key] = hcountList[key]

    outfile1.close()
    outfile2.close()

    hcountListNumbers["totalCount"] = tmp_count
    hcountList.clear()
    hcountList.update(tmp_hcountList)

# Parse chr-pos and find regions
def findAndWriteRegions(temporary, projectName, hcountList, hcountListNumbers, region_distance) :
    keys = hcountList.keys()
    keys.sort()
    
    regionArray = []
    for key in keys:
	regionArray.append(key)

    metaTag = temporary + 'temp.%s.region.meta.txt'%projectName
    countTag = temporary + 'temp.%s.region.count.txt'%projectName
    tailTag = temporary + 'temp.%s.region.tail.txt'%projectName
    outfile1 = open(metaTag, 'w')
    outfile2 = open(countTag, 'w')
    outfile3 = open(tailTag, 'w')

    regionCounter = 0
    lastPrint = 0
    i = 0
    while i < len(regionArray) :
	if (i+1) == len(regionArray) : #if starting position is the last element, then print the last element as both starting and ending and exit
	    writeTwosites(regionArray[i], regionArray[i], i, i, outfile1, outfile2, outfile3, projectName)
	    regionCounter += 1
	    break
	else :    
	    for j in range(i, len(regionArray)) :
		if (j+2) == len(regionArray) : #if the next element is the last element
		    if compareTwosites(regionArray[j], regionArray[j+1], region_distance) == 0 : #if it is within defined space then print together
			writeTwosites(regionArray[i], regionArray[j+1], i, j+1, outfile1, outfile2, outfile3, projectName)
			regionCounter += 1
			i = j + 2
			break
		    else : #if it is not within defined space then print previous and break
			writeTwosites(regionArray[i], regionArray[j], i, j, outfile1, outfile2, outfile3, projectName)
			regionCounter += 1
			i = j + 1
			break
		else :
		    if compareTwosites(regionArray[j], regionArray[j+1], region_distance) == 1 : #if it is not within defined space or same chromosome, then print and move on
			writeTwosites(regionArray[i], regionArray[j], i, j, outfile1, outfile2, outfile3, projectName)
			regionCounter += 1
			i = j + 1
			break
    
    outfile1.close()
    outfile2.close()
    outfile3.close()
    
    hcountListNumbers["regionCount"] =  regionCounter

# Compare two sites for distance
def compareTwosites(siteOne, siteTwo, region_distance) :
    oneChr, onePos = siteOne.split("-")
    twoChr, twoPos = siteTwo.split("-")
    
    if (oneChr == twoChr) :  #if same chromosome
	if (int(twoPos) - int(onePos)) <= int(region_distance) : #if Position is within defined distance
	    return 0

    return 1 #if Position is outside of defined distance OR not same chromosome   

# Print two sites for distance
def writeTwosites(siteOneC, siteTwoC, siteOneP, siteTwoP, outfile1, outfile2, outfile3, projectName) :
    chrNoOne, chrPosOne = siteOneC.split("-")
    chrNoTwo, chrPosTwo = siteTwoC.split("-")
    outfile1.write(chrNoOne)
    outfile1.write('\t')
    outfile1.write(chrPosOne)
    outfile1.write('\t')
    #outfile1.write(chrNoTwo)
    #outfile1.write('\t')
    outfile1.write(chrPosTwo)
    outfile1.write('\t')
    outfile1.write(projectName)
    outfile1.write('\n')

    outfile2.write(str(siteOneP))
    outfile2.write('\t')
    outfile2.write(str(siteTwoP))
    outfile2.write('\n')
    
    outfile3.write("+")
    outfile3.write('\t')
    outfile3.write(chrPosOne)
    outfile3.write('\t')
    outfile3.write(chrPosTwo)
    outfile3.write('\t')
    outfile3.write("0")
    outfile3.write('\t')
    outfile3.write(str((siteTwoP - siteOneP) + 1))
    outfile3.write('\n')    

# Removes all temporary files
def garbageCollector(temporary, projectName) :
    cmd = 'rm ' + temporary + 'temp.' + projectName + '.*'
    exitStatus = commands.getstatusoutput(cmd)
    return exitStatus[0]

########################################################################################################################
########################################################################################################################
# Main Function
########################################################################################################################
def msBayes2_main() :
    # Parse options
    usage='python msBayes2.py -options <addtional parameters>'
    description='parse HELP-tagging or Methyl-seq assay data and run OpenBUGS to determine methylation values'
    parser = argparse.ArgumentParser(usage=usage,description=description)
    parser.add_argument('-p', '--project', action="store", nargs=1, dest="project", metavar=("project"), help="project name")
    parser.add_argument('-s', '--scratch', action="store", nargs=1, dest="scratch", metavar=("scratch"), help="scratch space")
    parser.add_argument('-w', '--work', action="store", nargs=1, dest="work", metavar=("work"), help="work space")
    parser.add_argument("-f", "--files", action="store", nargs=2, dest="files", metavar=("msp_file", "hpaii_files"), help="mspI file name, hpaII file name")
    parser.add_argument('-d', '--filtered', dest="filtered", action="store_true")
    parser.add_argument("-b", "--msBayes2Site", action="store", nargs=1, dest="msBayes2Site", type=int, metavar="no_simulation", help="number of simulations")
    parser.add_argument("-r", "--msBayes2Region", action="store", nargs=2, dest="msBayes2Region", type=int, metavar=("no_simulation", "distance"), help="number of simulations, allowed distance between sites")
    args = parser.parse_args()

    # Check options
    if checkArguments(args) == 1 :
	sys.exit(1)
    else :
	# For storing information regarding project name and such
	infoList = {}
	infoList["projectName"] = args.project[0]
	infoList["scratch"] = args.scratch[0]
	infoList["temporary"] = args.work[0]
	infoList["msp"] = args.files[0]
	infoList["hpa"] = args.files[1]

	# For storing other useful information such as mspTotal and etc below
	hcountListNumbers = {}
	hcountListNumbers["mspCount"] = 0		
	hcountListNumbers["hpaCount"] = 0
	hcountListNumbers["totalCount"] = 0
	hcountListNumbers["mspTotal"] = 0
	hcountListNumbers["hpaTotal"] = 0
	hcountListNumbers["regionCount"] = 0
	hcountListNumbers["jobNumber"] = randomJob()

	# For storing chrNo-Pos and Msp and Hpa counts
	hcountList = {}

	# Read and check the file format
	if check_file_identify(infoList["temporary"] + infoList["msp"]) == 5 and check_file_identify(infoList["temporary"] + infoList["hpa"]) == 5 :
	    readMspHpaEinstein(infoList["temporary"] + infoList["msp"], infoList["temporary"] + infoList["hpa"], hcountList, hcountListNumbers)
	elif check_file_identify(infoList["temporary"] + infoList["msp"]) == 2 and check_file_identify(infoList["temporary"] + infoList["hpa"]) == 2 :
	    readMspHpaSimple(infoList["temporary"] + infoList["msp"], infoList["temporary"] + infoList["hpa"], hcountList, hcountListNumbers)
	else :
	    print("Incompatible file format")
	    sys.exit(1)

	# write temp files
	if args.filtered :
	    writeMspHpaCountsFiltered(infoList["temporary"], infoList["projectName"], hcountList, hcountListNumbers)
	else :
	    writeMspHpaCounts(infoList["temporary"], infoList["projectName"], hcountList)

	# Run	
	if args.msBayes2Site:
	    msBayes2Site(infoList, hcountListNumbers, args.msBayes2Site[0])
	if args.msBayes2Region:
	    msBayes2Region(infoList, hcountList, hcountListNumbers, args.msBayes2Region[0], args.msBayes2Region[1])

	garbageCollector(infoList["temporary"], infoList["projectName"])

########################################################################################################################
########################################################################################################################
# Run
######################################################################################################################## 
if __name__ == '__main__':
    print("Running msBayes2.py")
    msBayes2_main()
