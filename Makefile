CC = gcc
CFLAGS = -fopenmp

all: msBayes2Region msBayes2Site

msBayes2Region: msBayes2Region.c
	$(CC) $(CFLAGS) -o msBayes2Region msBayes2Region.c

msBayes2Site: msBayes2Site.c
	$(CC) $(CFLAGS) -o msBayes2Site msBayes2Site.c

clean:
	rm msBayes2Region msBayes2Site
